import { IsearchParam, Ipager } from '@/interface/globalTyping';

// --- API請求格式 ---

// /group/getList	取得群組列表
export interface IauthGroupList {
  condition: {
    groupName: string;
    orderRule: IsearchParam | [];
    beginAt: string;
    endAt: string;
  };
  pager: Ipager;
}

// /group/getCurrGroupUserList	取得特定群組使用者
export interface IauthGroupCurrGroupUserList {
  condition: {
    id: string;
    orderRule: IsearchParam | [];
  };
  pager: Ipager;
}

// /group/create	新增群組
export interface IcreateAuthGroup {
  groupName: string;
  authList: string[];
}

// /group/edit	編輯群組
export interface IeditAuthGroup {
  id: string;
  groupName: string;
  authList: string[];
}

// /group/remove	刪除群組
export interface IremoveAuthGroup {
  id: string;
}

// component 內
export interface IauthGroupSelector {
  value: string;
  label: string;
  isDisabled?: boolean;
}
