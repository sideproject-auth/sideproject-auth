import { IsearchParam, Ipager } from '@/interface/globalTyping';
// --- API請求格式 ---

// /auth/create	建立權限
export interface IauthCreate {
  id: string;
  authId: string;
  belongGroup: string;
  desc: string;
}

// /auth/edit	編輯權限
export interface IauthEdit {
  id: string;
  authId: string;
  belongGroup: string;
  desc: string;
}

// /auth/remove	刪除權限
export interface IauthRemove {
  id: string;
}

// /auth/getList	取得權限列表
// 暫時不需要
// export interface IuserList {}
