const baseApiUrl = process.env.VUE_APP_BASE_API;
const testApiUrl = process.env.TEST_API_URL;

export const defaultConfig = {
  rootUrl: baseApiUrl,
  headers: {
    'Content-Type': 'application/json;charset=utf-8',
  },
};

// custom
export const placeholderConfig = {
  rootUrl: testApiUrl,
  headers: {
    'Content-Type': 'text/json;charset=utf-8',
  },
};
