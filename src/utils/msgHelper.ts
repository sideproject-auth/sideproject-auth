import { endLoading } from '@/utils/loading';
import Vue from 'vue';
import { Message } from 'element-ui';

Vue.component(Message.name, Message);

// 狀態400時
function badResHelper(e: any) {
  const errMsg = e.response.data.message;

  Vue.prototype.$message({
    type: 'error',
    message: errMsg,
    duration: 2000,
  });

  // token如果過期，返回登入畫面
  if (errMsg === '無效驗證') {
    localStorage.clear();
    location.href = `//${location.host}`;
  }
  throw new Error();
}

// 其他訊息
function customMsgHelper(type: string = 'success', msg: string) {
  Vue.prototype.$message({
    type,
    message: msg,
    duration: 2000,
  });
}

export { badResHelper, customMsgHelper };
