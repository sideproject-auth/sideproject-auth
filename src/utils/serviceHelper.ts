import axios from 'axios';
import * as c from '@/utils/networkConfig';
import { Iresponse } from '@/interface/globalTyping';
import { badResHelper, customMsgHelper } from '@/utils/msgHelper';
import { startLoading, endLoading } from '@/utils/loading';

function checkMethodValue(method: string) {
  const positionMethods: string[] = ['get', 'post', 'put', 'delete', 'patch'];
  const isValidate: boolean = positionMethods.includes(method);

  if (!isValidate) {
    return false;
  }

  return true;
}

const beforeRequest = (config: string, apiUrl: string) => {
  let headers: any = {};
  if (config === 'defaultConfig') {
    headers = c[config].headers;
  }

  const headerConfig: any = {
    headers,
  };

  const token: string = localStorage.getItem('token') || '';

  if (token) {
    headers.Authorization = 'Bearer ' + token;
  }

  let fullApiUrl: string = '';
  if (typeof config === 'string') {
    fullApiUrl = c.defaultConfig.rootUrl + apiUrl;
  }

  return { headerConfig, fullApiUrl };
};

const ajaxRequest = async (method: string, apiUrl: string, payload: object, config: object) => {
  const requstMethods: any = {
    async get() {
      return await axios.get(apiUrl, config);
    },
    async post() {
      return await axios.post(apiUrl, payload, config);
    },
    async patch() {
      return await axios.patch(apiUrl, payload, config);
    },
    async put() {
      return await axios.put(apiUrl, payload, config);
    },
    // axios的delete不支援帶資料 ...
    async delete() {
      return await axios.request({
        method: 'delete',
        url: apiUrl,
        data: payload,
        headers: config,
      });
    },
  };

  // 顯示 loading spin

  let res: any;

  try {
    startLoading();
    res = await requstMethods[method]();
  } catch (e) {
    badResHelper(e);
  } finally {
    endLoading();
  }

  return res;
};

const afterRequest = (res: any): Iresponse | null => {
  if (!res) {
    return null;
  }

  // 如果有警告訊息顯示出來
  if (res.data.warning) {
    customMsgHelper('warning', res.data.warning);
  }

  // 過濾出需要的內容
  return {
    status: res.request.status,
    header: res.headers,
    code: res.data.code,
    data: res.data.data,
  };
};

/**
 *
 * @param method ajax方法
 * @param apiUrl api路徑(去除根url)
 * @param payload 參數
 * @param header 是否使用特別設定的頭部，預設看config檔案
 */
export const serviceRequest = async (method: any, apiUrl: any, payload: any = {}, header: string = 'defaultConfig') => {
  // 檢查HTTP Verbs是否正確
  const isValidate = checkMethodValue(method);
  if (!isValidate) return;

  // 設定header和api url
  const { headerConfig, fullApiUrl } = beforeRequest(header, apiUrl);

  // 送出request
  const response = await ajaxRequest(method, fullApiUrl, payload, headerConfig);

  // 回傳data
  return afterRequest(response);
};
