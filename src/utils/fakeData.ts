import {
  codeConvertHelper,
  formatValueHelper
} from '@/utils/codeConvertHelper';
import { cssClass } from '@/utils/getCsshelper';
const columnConfig: any[] = [
  {
    prop: 'date',
    label: '日期',
    width: 'auto',
    minWidth: '110',
    sortable: 'custom',
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 2
  },
  {
    prop: 'cardNumber',
    label: '卡號',
    width: 'auto',
    minWidth: '110',
    sortable: 'custom',
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 2
  },
  {
    prop: 'name',
    label: '姓名',
    width: 'auto',
    minWidth: '180',
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 2
  },
  {
    prop: 'checkIn',
    label: '上班',
    width: 'auto',
    minWidth: '110',
    sortable: 'custom',
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 2
  },
  {
    prop: 'checkOut',
    label: '下班',
    width: 'auto',
    minWidth: '110',
    sortable: 'custom',
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 2
  },
  {
    prop: 'dutyHours',
    label: '時數',
    width: 'auto',
    minWidth: '110',
    sortable: 'custom',
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 2
  },
  {
    prop: 'lateTime',
    label: '遲到',
    width: 'auto',
    minWidth: '110',
    sortable: 'custom',
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 2
  },
  {
    prop: 'earlyTime',
    label: '早退',
    width: 'auto',
    minWidth: '110',
    sortable: 'custom',
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 2
  },
  {
    prop: 'leaveType',
    label: '假別',
    width: 'auto',
    minWidth: '110',
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 2
  },
  {
    prop: 'remark',
    label: '備註',
    width: 'auto',
    minWidth: '110',
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 2
  }
];

const fakeData: any = [
  {
    date: '2019/08/11',
    cardNumber: '43289743',
    name: '朱自清(Mars)',
    checkIn: '09:18:32',
    checkOut: '18:30:18',
    dutyHours: '08:12:34',
    lateTime: '00:03:41',
    earlyTime: '00:00:08',
    leaveType: '病假(全天)',
    remark: ''
  },
  {
    date: '2019/08/11',
    cardNumber: '43289743',
    name: '朱自清(Mars)',
    checkIn: '09:18:32',
    checkOut: '18:30:18',
    dutyHours: '08:12:34',
    lateTime: '00:03:41',
    earlyTime: '00:00:08',
    leaveType: '病假(全天)',
    remark: ''
  },
  {
    date: '2019/08/11',
    cardNumber: '43289743',
    name: '朱自清(Mars)',
    checkIn: '09:18:32',
    checkOut: '18:30:18',
    dutyHours: '08:12:34',
    lateTime: '00:03:41',
    earlyTime: '00:00:08',
    leaveType: '病假(全天)',
    remark: ''
  },
  {
    date: '2019/08/11',
    cardNumber: '43289743',
    name: '朱自清(Mars)',
    checkIn: '09:18:32',
    checkOut: '18:30:18',
    dutyHours: '08:12:34',
    lateTime: '00:03:41',
    earlyTime: '00:00:08',
    leaveType: '病假(全天)',
    remark: ''
  },
  {
    date: '2019/08/11',
    cardNumber: '43289743',
    name: '朱自清(Mars)',
    checkIn: '09:18:32',
    checkOut: '18:30:18',
    dutyHours: '08:12:34',
    lateTime: '00:03:41',
    earlyTime: '00:00:08',
    leaveType: '病假(全天)',
    remark: ''
  },
  {
    date: '2019/08/11',
    cardNumber: '43289743',
    name: '朱自清(Mars)',
    checkIn: '09:18:32',
    checkOut: '18:30:18',
    dutyHours: '08:12:34',
    lateTime: '00:03:41',
    earlyTime: '00:00:08',
    leaveType: '病假(全天)',
    remark: ''
  },
  {
    date: '2019/08/11',
    cardNumber: '43289743',
    name: '朱自清(Mars)',
    checkIn: '09:18:32',
    checkOut: '18:30:18',
    dutyHours: '08:12:34',
    lateTime: '00:03:41',
    earlyTime: '00:00:08',
    leaveType: '病假(全天)',
    remark: ''
  }
];

export { columnConfig, fakeData };
