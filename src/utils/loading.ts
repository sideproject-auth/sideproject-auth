import { Loading } from 'element-ui';
let loadingInstance: any = {};
import Vue from 'vue';

const startLoading = () => {
  loadingInstance = Loading.service({});
};

const endLoading = () => {
  Vue.prototype.$nextTick(() => {
    loadingInstance.close();
  });
};

export { startLoading, endLoading };
