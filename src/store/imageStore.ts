// 引入type，避免function衝突
import * as type from '@/store/types';

import ImageService from '@/services/imageService';
const imageService = new ImageService();

export default {
  namespaced: true,
  state: {
    imageList: []
  },
  getters: {},
  mutations: {
    [type.imageList](state: any, payload: any) {
      state.imageList = payload;
    }
  },
  actions: {
    async getImageList({ commit }: any, payload: any) {
      const res: any = await imageService.getImageListAPI(payload);
      commit(type.imageList, res.data);
    }
  }
};
