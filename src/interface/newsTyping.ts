import { IsearchParam, Ipager } from '@/interface/globalTyping';

// --- API請求格式 ---

// /news/getList	取得消息內容以外的所有資訊列表

export interface IgetNewsList {
  condition: {
    title: string;
    orderRule: IsearchParam | [];
    beginAt: string;
    endAt: string;
    showAll: boolean; // true 顯示全部公開與不公開, false 指顯示公開文章
  };
  pager: Ipager;
}

// /news/content	取得單篇消息所有內容
export interface IgetNews {
  id: string;
}
