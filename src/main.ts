// dependencies & package
import Vue from 'vue';
import App from '@/App.vue';
Vue.config.productionTip = false; // 關掉console裡面的提示

// router/store
import router from '@/router';
import store from '@/store';

// i18n
import VueI18n from 'vue-i18n';
import i18nData from '@/i18n';

import ElementUI from 'element-ui';
Vue.use(ElementUI);
/* 改变 icon 字体路径变量，必需 */

import 'element-ui/lib/theme-chalk/index.css';

// vue meta
import VueMeta from 'vue-meta';

Vue.use(VueI18n);
const i18n = new VueI18n({
  locale: 'zh-tw' || 'en',
  messages: i18nData
});

Vue.use(VueMeta, {
  refreshOnceOnNavigation: true
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app');
