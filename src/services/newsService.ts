import { serviceRequest } from '@/utils/serviceHelper';

class NewsService {
  public getNewsListAPI(payload = {}) {
    const apiUrl = '/news/getList';
    return serviceRequest('post', apiUrl, payload);
  }

  public getNewsAPI(payload = {}) {
    const apiUrl = '/news/getNews';
    return serviceRequest('post', apiUrl, payload);
  }
}

export default NewsService;
