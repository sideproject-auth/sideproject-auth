import { serviceRequest } from '@/utils/serviceHelper';

class ImageService {
  public getImageListAPI(payload = {}) {
    const apiUrl = '/image/getList';
    return serviceRequest('post', apiUrl, payload);
  }
}

export default ImageService;
