import { serviceRequest } from '@/utils/serviceHelper';

export const handleTestAPI = (payload = {}) => {
  const apiUrl = '/todos/1';
  return serviceRequest('get', apiUrl, payload, 'placeholderConfig');
};
