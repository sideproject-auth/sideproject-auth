export default [
  {
    path: '*',
    name: 'page404',
    component: () => import('@/views/Page404.vue'),
  },
];
