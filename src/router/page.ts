import Home from '@/views/Home.vue';
export default [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/allNews',
    name: 'news',
    component: () => import('@/views/AllNews.vue')
  },
  {
    path: '/news/:id',
    name: 'fullNews',
    component: () => import('@/views/FullNews.vue')
  },
  {
    path: '*',
    name: 'page404',
    component: () => import('@/views/Page404.vue')
  }
];
