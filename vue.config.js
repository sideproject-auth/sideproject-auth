module.exports = {
  devServer: {
    progress: false,
    port: 7200,
    overlay: {
      warnings: false,
      errors: true,
    },
  },
};
